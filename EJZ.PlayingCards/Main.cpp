#include <iostream>
#include <conio.h>

enum Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

enum suit
{
	DIAMOND = 0,
	SPADE = 0,
	HEARTS = 0,
	CLUBS = 0
};

struct Card
{
	Rank cardRank;
	suit cardSuit;
};

int main()
{
	Card c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19,
		c20, c21, c22, c23, c24, c25, c26, c27, c28, c29, c30, c31, c32, c33, c34, c35, c36,
		c37, c38, c39, c40, c41, c42, c43, c44, c45, c46, c47, c48, c49, c50, c51, c52;
	//Diamond Cards.
	c1.cardRank = TWO; c1.cardSuit = DIAMOND;
	c2.cardRank = THREE; c2.cardSuit = DIAMOND;
	c3.cardRank = FOUR; c3.cardSuit = DIAMOND;
	c4.cardRank = FIVE; c4.cardSuit = DIAMOND;
	c5.cardRank = SIX; c5.cardSuit = DIAMOND;
	c6.cardRank = SEVEN; c6.cardSuit = DIAMOND;
	c7.cardRank = EIGHT; c7.cardSuit = DIAMOND;
	c8.cardRank = NINE; c8.cardSuit = DIAMOND;
	c9.cardRank = TEN; c9.cardSuit = DIAMOND;
	c10.cardRank = JACK; c10.cardSuit = DIAMOND;
	c11.cardRank = QUEEN; c11.cardSuit = DIAMOND;
	c12.cardRank = KING; c12.cardSuit = DIAMOND;
	c13.cardRank = ACE; c13.cardSuit = DIAMOND;
	//Spade Cards.
	c14.cardRank = TWO; c14.cardSuit = SPADE;
	c15.cardRank = THREE; c15.cardSuit = SPADE;
	c16.cardRank = FOUR; c16.cardSuit = SPADE;
	c17.cardRank = FIVE; c17.cardSuit = SPADE;
	c18.cardRank = SIX; c18.cardSuit = SPADE;
	c19.cardRank = SEVEN; c19.cardSuit = SPADE;
	c20.cardRank = EIGHT; c20.cardSuit = SPADE;
	c21.cardRank = NINE; c21.cardSuit = SPADE;
	c22.cardRank = TEN; c22.cardSuit = SPADE;
	c23.cardRank = JACK; c23.cardSuit = SPADE;
	c24.cardRank = QUEEN; c24.cardSuit = SPADE;
	c25.cardRank = KING; c25.cardSuit = SPADE;
	c26.cardRank = ACE; c26.cardSuit = SPADE;
	//Heart Cards.
	c27.cardRank = TWO; c27.cardSuit = HEARTS;
	c28.cardRank = THREE; c28.cardSuit = HEARTS;
	c29.cardRank = FOUR; c29.cardSuit = HEARTS;
	c30.cardRank = FIVE; c30.cardSuit = HEARTS;
	c31.cardRank = SIX; c31.cardSuit = HEARTS;
	c32.cardRank = SEVEN; c32.cardSuit = HEARTS;
	c33.cardRank = EIGHT; c33.cardSuit = HEARTS;
	c34.cardRank = NINE; c34.cardSuit = HEARTS;
	c35.cardRank = TEN; c35.cardSuit = HEARTS;
	c36.cardRank = JACK; c36.cardSuit = HEARTS;
	c37.cardRank = QUEEN; c37.cardSuit = HEARTS;
	c38.cardRank = KING; c38.cardSuit = HEARTS;
	c39.cardRank = ACE; c39.cardSuit = HEARTS;
	//Clubs Cards.
	c40.cardRank = TWO; c40.cardSuit = CLUBS;
	c41.cardRank = THREE; c41.cardSuit = CLUBS;
	c42.cardRank = FOUR; c42.cardSuit = CLUBS;
	c43.cardRank = FIVE; c43.cardSuit = CLUBS;
	c44.cardRank = SIX; c44.cardSuit = CLUBS;
	c45.cardRank = SEVEN; c45.cardSuit = CLUBS;
	c46.cardRank = EIGHT; c46.cardSuit = CLUBS;
	c47.cardRank = NINE; c47.cardSuit = CLUBS;
	c48.cardRank = TEN; c48.cardSuit = CLUBS;
	c49.cardRank = JACK; c49.cardSuit = CLUBS;
	c50.cardRank = QUEEN; c50.cardSuit = CLUBS;
	c51.cardRank = KING; c51.cardSuit = CLUBS;
	c52.cardRank = ACE; c52.cardSuit = CLUBS;

	_getch();
	return 0;
}